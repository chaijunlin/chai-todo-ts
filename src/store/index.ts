import { createStore } from 'vuex'
import state from './babel/state'
import mutations from './babel/mutations'
import actions from './babel/actions'

export default createStore({
  state,
  mutations,
  actions
})
